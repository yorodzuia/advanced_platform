﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ItemBase))]
public class ItemBaseEditor : Editor
{
    private ItemBase itembase;

    private void Awake()
    {
        itembase = (ItemBase)target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("New Item"))
            itembase.CreateItem();
        if (GUILayout.Button("Remove Item"))
            itembase.RemoveItem();
        if (GUILayout.Button("<="))
            itembase.PrevItem();
        if (GUILayout.Button("=>"))
            itembase.NextItem();

        GUILayout.EndHorizontal();

        base.OnInspectorGUI();
    }
}
