﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singlton
    public static GameManager Instance {get; private set;}
    #endregion

    [SerializeField] private GameObject InventoryPanel;
    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinContainer;
    public Dictionary<GameObject, BuffReciever> buffReceiverContainer;
    public Dictionary<GameObject, ItemComponent> itemContainer;
    [HideInInspector] public PlayerInventory Inventory;
    public ItemBase itemDataBase;

    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        buffReceiverContainer = new Dictionary<GameObject, BuffReciever>();
        itemContainer = new Dictionary<GameObject, ItemComponent>();
    }

    public void OnClickPause()
    {
        if (Time.timeScale > 0)
        {
            InventoryPanel.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            InventoryPanel.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
}
