﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] private InputField nameField;
    private string playerNameKey = "Player_Name";

    private void Start()
    {
        if (PlayerPrefs.HasKey(playerNameKey))
            nameField.text = PlayerPrefs.GetString(playerNameKey);
    }
    public void onEndEditName()
    {
        PlayerPrefs.SetString(playerNameKey,nameField.text);
    }
    public void onClickPlay()
    {
        SceneManager.LoadScene(1);
    }

    public void onClickExit()
    {
        Application.Quit();
    }
}
