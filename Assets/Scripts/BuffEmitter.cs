﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffEmitter : MonoBehaviour
{
    public Buff buff;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.buffReceiverContainer.ContainsKey(col.gameObject))
        {
            var receiver = GameManager.Instance.buffReceiverContainer[col.gameObject];
            receiver.AddBuff(buff);
        }
    }
    private void OnTriggerExit2D(Collider2D col)
    {
        if (GameManager.Instance.buffReceiverContainer.ContainsKey(col.gameObject))
        {
            var receiver = GameManager.Instance.buffReceiverContainer[col.gameObject];
            receiver.RemoveBuff(buff);
        }
    }
}
[System.Serializable]
public class Buff
{
    public Bufftype type;
    public float additiveBonus;
    public float multipleBonus;
}

public enum Bufftype : byte
{
    Damage, Armor, Force
}