﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float speed = 2.5f;
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }
    [SerializeField] private float force;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private float minimalHaight;
    [SerializeField] private bool isCheatMode;
    [SerializeField] private GroundDetection groundDetection;
    [SerializeField] private Vector3 direction;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float shootForce = 5;
    [SerializeField] private Arrow arrow;
    [SerializeField] private float cooldown;
    [SerializeField] private int arrowCount = 3;
    [SerializeField] private Health health;
    [SerializeField] private Item item;
    [SerializeField] private BuffReciever buffReciever;
    [SerializeField] private float getDamageForce;
    [SerializeField] private Camera playerCamera;
    
    public Health Health 
    { 
        get { return health;}
    }
    private List<Arrow> arrowPool;
    private Arrow currentArrow;
    private bool isCooldown;
    private bool isJumping;
    private float bonusForce;
    private float bonusArmor;
    private float bonusDamage;
    private bool isBlockMovement;
    private UICharacterController uiController;

    public static Player Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        
        arrowPool = new List<Arrow>();
        for (int i = 0; i < arrowCount; i++)
        { 
        var arrowTemp = Instantiate (arrow, arrowSpawnPoint);
            arrowPool.Add(arrowTemp);
            arrowTemp.gameObject.SetActive(false);
        }

        health.OnTakeHit += TakeHit;
        buffReciever.onBuffsChanged += ApplyBuffs;
    }

    private void ApplyBuffs()
    {
        var forceBuff = buffReciever.Buffs.Find(i => i.type == Bufftype.Force);
        var damageBuff = buffReciever.Buffs.Find(i => i.type == Bufftype.Damage);
        var armorBuff = buffReciever.Buffs.Find(i => i.type == Bufftype.Armor);
        bonusForce = forceBuff == null ? 0 : forceBuff.additiveBonus;
        bonusDamage = damageBuff == null ? 0 : damageBuff.additiveBonus;
        bonusArmor = armorBuff == null ? 0 : armorBuff.additiveBonus;
        health.SetHealth((int)bonusArmor);
    }
    void FixedUpdate()
    {
        Move();
        animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));
        CheckFall();    
    }
    private void Jump()
    {
        if (groundDetection.isGrounded)
        {
            rigidbody.AddForce(Vector2.up * (force + bonusForce), ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
        }

    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
            Jump();
        if (Input.GetKeyDown(KeyCode.Escape))
            GameManager.Instance.OnClickPause();
#endif
    }

    private void Move()
    {
        animator.SetBool("isGrounded", groundDetection.isGrounded);
        if (!isJumping && !groundDetection.isGrounded)
        {
            animator.SetTrigger("SrartFall");
        }
        isJumping = isJumping && !groundDetection.isGrounded;
        direction = Vector3.zero;
#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left;
        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right;
#endif
        if (uiController.Left.IsPressed)
            direction = Vector3.left;
        if (uiController.Right.IsPressed)
            direction = Vector3.right;
        direction *= speed;
        direction.y = rigidbody.velocity.y;
        if (!isBlockMovement)
        rigidbody.velocity = direction;



        if (direction.x > 0)
            spriteRenderer.flipX = false;
        if (direction.x < 0)
            spriteRenderer.flipX = true;
    }
    void CheckShoot()
    {
        if (!isCooldown)
        {
            animator.SetTrigger("StartShoot");
        }
    }

    public void InitArrow() 
    {
        currentArrow = GetArrowFromPool();
        currentArrow.SetImpulse(Vector2.right,0,0,this);
        
    }

    private void Shoot()
    {
        currentArrow.SetImpulse(Vector2.right, 
            spriteRenderer.flipX ? -force * shootForce : force * shootForce, (int)bonusDamage, this);
        StartCoroutine(Cooldown());
    }

    private IEnumerator Cooldown()
    {
        isCooldown = true;
        yield return new WaitForSeconds(cooldown);
        isCooldown = false;
        yield break;
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var arrowTemp = arrowPool[0];
            arrowPool.Remove(arrowTemp);
            arrowTemp.gameObject.SetActive(true);
            arrowTemp.transform.parent = null;
            arrowTemp.transform.position = arrowSpawnPoint.transform.position;
            return arrowTemp;
        }
        else
        return Instantiate(arrow, arrowSpawnPoint.position, Quaternion.identity);
        
    }

    public void ReturArrowToPool(Arrow arrowTemp)
    {
        if (!arrowPool.Contains(arrowTemp))
            arrowPool.Add(arrowTemp);

        arrowTemp.transform.parent = arrowSpawnPoint;
        arrowTemp.transform.position = arrowSpawnPoint.transform.position;
        arrowTemp.gameObject.SetActive(false);
    }
    void CheckFall()
    {
        if (transform.position.y < minimalHaight && isCheatMode)
        {
            rigidbody.velocity = new Vector2(x: 0, y: 0);
            transform.position = new Vector3(x: 0, y: 0, z: 0);
        }

        else if (transform.position.y < minimalHaight && !isCheatMode)
            Destroy(gameObject);
    }

    public void InitUIController(UICharacterController UIcontroller)
    {
        uiController = UIcontroller;
        uiController.Jump.onClick.AddListener(Jump);
        uiController.Fire.onClick.AddListener(CheckShoot);
    }

    private void TakeHit(int damage, GameObject attaker)
    {
        animator.SetBool("GetDamage", true);
        animator.SetTrigger("TakeHit");
        isBlockMovement = true;
        rigidbody.AddForce(transform.position.x < attaker.transform.position.x ?
            new Vector2(-getDamageForce, 0) : new Vector2(getDamageForce, 0), ForceMode2D.Impulse);
    }

    public void UnBlockMovement()
    {
        isBlockMovement = false;
        animator.SetBool("GetDamage", false);
    }

    private void OnDestroy()
    {
        playerCamera.transform.parent = null;
        playerCamera.enabled = true;
    }
}