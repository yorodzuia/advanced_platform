﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour
{
    public int coinsCount;
    [SerializeField] private Text cointsText;
    private List<Item> items;
    public BuffReciever buffReciever;

    public List<Item> Items
    { get { return items; } }

    private void Start()
    {
        GameManager.Instance.Inventory = this;
        cointsText.text = coinsCount.ToString();
        items = new List<Item>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.coinContainer.ContainsKey(col.gameObject))
        {
            coinsCount++;
            cointsText.text = coinsCount.ToString();
            var coin = GameManager.Instance.coinContainer[col.gameObject];
            coin.StartDestroy();
        }
        if (GameManager.Instance.itemContainer.ContainsKey(col.gameObject))
        {
            var itemComponent = GameManager.Instance.itemContainer[col.gameObject];
            items.Add(itemComponent.Item);
            itemComponent.Destroy(col.gameObject);
        }
    }
}
