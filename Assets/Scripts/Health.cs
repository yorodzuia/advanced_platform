﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float health;
    public Action<int,GameObject> OnTakeHit;
    public float CurrentHealth
    { 
        get { return health; } 
    }

    private void Start()
    {
        GameManager.Instance.healthContainer.Add(gameObject, this);
    }
    public void TakeHit(int damage, GameObject attaker)
    {
        health -= damage;

        if (OnTakeHit != null)
            OnTakeHit(damage,attaker);

        if (health <= 0)
            Destroy(gameObject);
    }

    public void SetHealth(int bonushealth)
    {
        health += bonushealth;
    }
}
