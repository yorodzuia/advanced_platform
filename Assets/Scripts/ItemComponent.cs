﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour, IObjectDestroyer
{
    [SerializeField] private ItemType itemType;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Animator animator;
    private Item item;

    public Item Item
    { get { return item; } }

    public void Destroy(GameObject gameObject)
    {
        animator.SetTrigger("StartDestroy");
    }
    
    public void EndDestroy()
    {
        MonoBehaviour.Destroy(gameObject);
    }

    void Start()
    {
        item = GameManager.Instance.itemDataBase.GetOfItemId((int)itemType);
        spriteRenderer.sprite = item.Icon;
        GameManager.Instance.itemContainer.Add(gameObject, this);
    }
}

public enum ItemType
{
    ArmorPotion = 0, 
    DamagePoion = 1,
    ForcePotion = 2
}